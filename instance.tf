resource "aws_instance" "awstrainingwithjagan_ubuntu" {
  ami           = var.ami_id  # Ubuntu Server 20.04 LTS AMI in us-west-2; please update if needed or for a different region
  instance_type = var.instance_type
  security_groups = [aws_security_group.allow_ssh.name]  # Reference the security group created above

  tags = {
    Name = var.instance_name
  }
}
