variable "ami_id" {
  description = "ami id"
  type        = string
  default     = "ami-04b70fa74e45c3917"
}
variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "awstrainingwithjaganubuntu"
}
variable "instance_type" {
  description = "instance type"
  type        = string
  default     = "t2.micro"
}