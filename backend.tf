terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

backend "s3" {
    bucket = "terraformstateawsdemo"
    key    = "tf_state.tf"
    region = "us-east-1"  # Change this to your bucket's region
  }
}  
provider "aws" {
  region = "us-east-1"  # Change this to your preferred region
}
